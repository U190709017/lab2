public class FindLetterGrade {    
    public static void main(String []args) {
    
        int x1 = Integer.parseInt(args[0]);    
        if (x1>100 || x1<0)
            System.out.println("It is not a valid score!");
        else if (x1 <= 100 && x1 >= 90)
            System.out.println("Your Grade is A");
        else if (x1 < 90 && x1 >=80) 
            System.out.println("Your Grade is B");
        else if (x1 < 80 && x1 >= 70)
            System.out.println("Your Grade is C");
        else if (x1 < 70 && x1 >=60)
            System.out.println("Your Grade is D");
        else if (x1 < 60 && x1 >= 0)
            System.out.println("Your Grade is F");


    }
}


