import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

	public static void main(String[] args) throws IOException {	
		Scanner reader = new Scanner(System.in); 
		Random rand = new Random(); 
		int number =rand.nextInt(100);
		
		
		System.out.println("Hi! I'm thinking of a number between 0 and 99.");
		System.out.print("Can you guess it: ");
		
		int guess = reader.nextInt();
        int c = 0;
        do{
            System.out.print("type -1 or try another shot ");
            guess = reader.nextInt();
            c++;
            if ( guess == -1)
                System.out.println("sry, the number was: " + number);
            else if (guess == number)
                System.out.println("True,You're tried to guess number "+c+" time!");
            else{    
                if (guess < number)
                    System.out.println("Your guess is higher than the number");
                else if ( guess > number)
                    System.out.println("Your guess is lower than the number");
            }        
        }while((number != guess)&&(guess != (-1)));
    				
		reader.close();
	}
	
	
}
