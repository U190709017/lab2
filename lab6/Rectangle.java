public class Rectangle{
    int sideA;
    int sideB;
    Point topleft;
    
    public Rectangle(Point p,int x,int y){
        this.sideA=x;
        this.sideB=y;
        this.topleft=p;
    }
    public int area(){
        return(this.sideA*this.sideB);
    }
    public int perimeter(){
        return(2*(this.sideA+this.sideB));
    }
    public Point[] corners(){
        Point topright=new Point(topleft.xCoord+this.sideA,topleft.yCoord);
        Point altleft=new Point(topleft.xCoord,topleft.yCoord-this.sideB);
        Point altright=new Point(altleft.xCoord+this.sideA,altleft.yCoord);

        Point[] array1=new Point[4];
        array1[0]=this.topleft;
        array1[1]=topright;
        array1[2]=altright;
        array1[3]=altleft;
        return(array1);
    }


    public static void main(String[] args){
        Point p = new Point(5,5);
        Rectangle r = new Rectangle(p,5,5);
        System.out.println(r.area());
        System.out.println(r.perimeter());
       
        Point[] point1 = r.corners();
        for(int i=0;i<4;i++){
            System.out.println(point1[i].xCoord+" "+point1[i].yCoord);
        }
    }
}



